# golang-cknit

#### 介绍
使用 GoLang 语言编写的秒级定时器，支持秒级和分钟级别定时器和任务管理，并且可以保证在机器硬盘没问题的情况下保证数据安全落地

#### 应用场景
在各个公司里面都有各种类型的定时任务需求，而 `golang-cknit`就是为了解决这个问题而诞生的，结合作者本身的工作经历会逐渐丰富此软件的功能，如：支持定时任务的参数等

#### 软件架构
延续了 `C` 语言版本的 `cknit` 的特点，采用 `golang` 的 `goroutine` 特性完成任务调度问题，并且支持通过API接口操作定时任务

#### 发布日志

|版本|内容|
|----|----|
|v1.0| 提供持久化和接口处理定时任务，不支持分布式 |
|v2.0| 分布式支持 |

#### 安装教程

1. 安装 `golang` 环境

Debian系：

`apt install golang -y `

CentOS 系:

`yum install golang -y`

2. 编译和安装

   `git clone https://gitee.com/quzhui/golang-cknit.git`

   `cd golang-cknit`

   `go build -o cknit main.go`

   `cp -r main /usr/bin/cknit`

####  定时规则

定时器采用 `crontab` 格式的定时规则，具体规则细则如下：

| 表达式 | 规则                    |
| ------ | ----------------------- |
| *      | 表示都成立              |
| */1    | 表示每隔`1`单位成立     |
| 12,3   | 表示等于12和3的时候成立 |
| 1-3,4  | 表示1到3和4都成立       |

`golang-cknit` 支持两种条件的定时规则：

`* * * * * `: 由四个空格隔开的条件，组成的 `分`、`时`、`日`、`月`、`周`

以及由五个空格隔开：

`* * * * * *`的 `秒`、`分`、`时`、`日`、`月`、`周`



#### APIs说明

1.  添加定时任务：

| 端口         | 8080                   |
| ------------ | ---------------------- |
| 请求方法     | POST                   |
| 请求包体数据 | JSON                   |
| 请求URL      | http://your_ip:8080/do |

```json
{
    "id":2,
    "name":"同步定向包",
    "val":"* * * * * *",
    "app":"/usr/bin/php /Users/josin/Desktop/php.php"
}
```



2. 修改任务

| 端口         | 8080                   |
| ------------ | ---------------------- |
| 请求方法     | PUT                    |
| 请求包体数据 | JSON                   |
| 请求URL      | http://your_ip:8080/do |

```json
{
    "id":3,
    "name":"同步定向包",
    "val":"* * * * * *",
    "app":"/usr/bin/php /Users/josin/Desktop/php.php"
}
```



3. 删除任务

| 端口         | 8080                   |
| ------------ | ---------------------- |
| 请求方法     | DELETE                 |
| 请求包体数据 | JSON                   |
| 请求URL      | http://your_ip:8080/do |

```json
{
    "id":2
}
```



4. 查询当前的所有定时任务

| 端口         | 8080                   |
| ---- | ---- |
| 请求方法     | GET                    |
| 请求包体数据 | JSON                   |
| 请求URL      | http://your_ip:8080/do |

```json
[
  {
    "id":1,
    "name":"同步定向包",
    "val":"* * * * * *",
    "app":"/usr/bin/php /Users/josin/Desktop/php.php"
  }
]
```

